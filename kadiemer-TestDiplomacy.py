#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, move, support, attack

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        lst = diplomacy_read(s)
        self.assertEqual(lst,  ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid'])

    def test_read_2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n"
        lst = diplomacy_read(s)
        self.assertEqual(lst,  ['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B'])

    def test_read_3(self):
        s = "A Austin Support B\nB Houston Support A\nC Dallas Move Austin\nD SanAntonio Move Houston\n"
        lst = diplomacy_read(s)
        self.assertEqual(lst,  ['A Austin Support B', 'B Houston Support A', 'C Dallas Move Austin', 'D SanAntonio Move Houston'])


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print({'C': '[dead]', 'A': '[dead]', 'B': '[dead]'}, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print({'A': 'Austin', 'C': '[dead]', 'B': 'LosAngeles'}, w)
        self.assertEqual(w.getvalue(), "A Austin\nB LosAngeles\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print({'A': 'Madrid'}, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    # -----
    # move
    # -----


    def test_move_1(self):
        inputVals = [['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'] , {'Madrid': ['A'], 'Barcelona': ['B'], 'London': ['C'], 'Paris': ['D'], 'Austin': ['E']}]
        expectedOutput = {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E']}
        output = move(inputVals[0],inputVals[1])
        self.assertEqual(output,expectedOutput)

    def test_move_2(self):
        inputVals = [['A Madrid Support C', 'B Barcelona Hold', 'C London Move Madrid'] , {'Madrid': ['A'], 'Barcelona': ['B'], 'London': ['C']}]
        expectedOutput = {'Madrid': ['A', 'C'], 'Barcelona': ['B'], 'London': []}
        output = move(inputVals[0],inputVals[1])
        self.assertEqual(output,expectedOutput)

    def test_move_3(self):
        inputVals = [['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A', 'F NewYork Move Austin'] , {'Madrid': ['A'], 'Barcelona': ['B'], 'London': ['C'], 'Paris': ['D'], 'Austin': ['E'], 'NewYork': ['F']}]
        expectedOutput = {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E', 'F'], 'NewYork': []}
        output = move(inputVals[0],inputVals[1])
        self.assertEqual(output,expectedOutput)
    # -----
    # supprt
    # -----


    def test_support_1(self):
        inputVals = [['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A'] , {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0} , {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E']}]
        expectedOutput = {'A': 1, 'B': 1, 'C': 0, 'D': 0, 'E': 0}
        output = support(inputVals[0],inputVals[1],inputVals[2])
        self.assertEqual(output,expectedOutput)

    def test_support_2(self):
        inputVals = [['A Madrid Support C', 'B Barcelona Hold', 'C London Move Madrid'] , {'A': 0, 'B': 0, 'C': 0} , {'Madrid': ['A', 'C'], 'Barcelona': ['B'], 'London': []}]
        expectedOutput = {'A': 0, 'B': 0, 'C': 0}
        output = support(inputVals[0],inputVals[1],inputVals[2])
        self.assertEqual(output,expectedOutput)

    def test_support_3(self):
        inputVals = [['A Madrid Hold', 'B Barcelona Move Madrid', 'C London Move Madrid', 'D Paris Support B', 'E Austin Support A', 'F NewYork Move Austin'] , {'A': 0, 'B': 0, 'C': 0, 'D': 0, 'E': 0, 'F': 0} , {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E', 'F'], 'NewYork': []}]
        expectedOutput = {'A': 0, 'B': 1, 'C': 0, 'D': 0, 'E': 0, 'F': 0}
        output = support(inputVals[0],inputVals[1],inputVals[2])
        self.assertEqual(output,expectedOutput)

    # -----
    # attack
    # -----


    def test_attack_1(self):
        inputVals = [['A', 'B', 'C', 'D', 'E'] , [1, 1, 0, 0, 0] , {'A': 1, 'B': 1, 'C': 0, 'D': 0, 'E': 0} , [] , {} , {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E']}]
        expectedOutput = {'A': '[dead]', 'B': '[dead]', 'C': '[dead]', 'D': 'Paris', 'E': 'Austin'}
        output = attack(inputVals[0],inputVals[1],inputVals[2],inputVals[3],inputVals[4],inputVals[5])
        self.assertEqual(output,expectedOutput)

    def test_attack_2(self):
        inputVals = [['A', 'B', 'C'] , [0, 0, 0] , {'A': 0, 'B': 0, 'C': 0} , [] , {} , {'Madrid': ['A', 'C'], 'Barcelona': ['B'], 'London': []}]
        expectedOutput = {'A': '[dead]', 'C': '[dead]', 'B': 'Barcelona'}
        output = attack(inputVals[0],inputVals[1],inputVals[2],inputVals[3],inputVals[4],inputVals[5])
        self.assertEqual(output,expectedOutput)

    def test_attack_3(self):
        inputVals = [['A', 'B', 'C', 'D', 'E', 'F'] , [0, 1, 0, 0, 0, 0] , {'A': 0, 'B': 1, 'C': 0, 'D': 0, 'E': 0, 'F': 0} , [] , {} , {'Madrid': ['A', 'B', 'C'], 'Barcelona': [], 'London': [], 'Paris': ['D'], 'Austin': ['E', 'F'], 'NewYork': []}]
        expectedOutput = {'A': '[dead]', 'B': 'Madrid', 'C': '[dead]', 'D': 'Paris', 'E': '[dead]', 'F': '[dead]'}
        output = attack(inputVals[0],inputVals[1],inputVals[2],inputVals[3],inputVals[4],inputVals[5])
        self.assertEqual(output,expectedOutput)
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Support C\nB Barcelona Hold\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE [dead]\nF [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
..................
----------------------------------------------------------------------
Ran 18 tests in 0.306s


OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          65      0     48      0   100%
TestDiplomacy.py      90      0      0      0   100%
--------------------------------------------------------------
TOTAL                155      0     48      0   100%
"""

